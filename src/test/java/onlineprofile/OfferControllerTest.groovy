package onlineprofile

import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.setup.MockMvcBuilders
import spock.lang.Specification

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status


class OfferControllerTest extends Specification {

    MockMvc mockMvc;

    OfferRepositoryV2 offerV2Repository = Stub()
    OfferRepository offerRepository = Stub()

    OfferController offerController =
            new OfferController(offerRepository, offerV2Repository);

    def setup() {
        mockMvc = MockMvcBuilders.standaloneSetup(offerController).build()
    }

    def 'should return 204 no content when there are no offers'() {
        given:
            offerRepository.noOffers() >> true
        expect:
            mockMvc.perform(get("/offers")
                    .accept("application/vnd.cards+json"))
            .andExpect(status().isNoContent())
    }

    def 'should return 200 when there are  offers'() {
        given:
            offerRepository.noOffers() >> false
            offerRepository.all() >> [new Offer(1, 2)]
        expect:
            mockMvc.perform(get("/offers")
                    .accept("application/vnd.cards+json"))
                    .andExpect(status().isOk())
                    .andExpect(content().json('[{"offerId":1, "fee":2}]'))
    }
}
