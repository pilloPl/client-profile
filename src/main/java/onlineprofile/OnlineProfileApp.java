package onlineprofile;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.hateoas.ResourceSupport;
import org.springframework.http.CacheControl;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.util.Map;
import java.util.Random;
import java.util.concurrent.ConcurrentHashMap;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.*;

@SpringBootApplication
public class OnlineProfileApp {

    public static void main(String[] args) {
        SpringApplication.run(OnlineProfileApp.class, args);
    }
}

@RestController
class OfferController {

    private final OfferRepository offerRepository;
    private final OfferRepositoryV2 offerV2Repository;

    @Autowired
    public OfferController(OfferRepository offerRepository, OfferRepositoryV2 offerV2Repository) {
        this.offerRepository = offerRepository;
        this.offerV2Repository = offerV2Repository;

    }

    @GetMapping(value = "/offers", produces = "application/vnd.cards+json")
    public ResponseEntity getOffers() {
        if (offerRepository.noOffers()) {
            return ResponseEntity.noContent().build();
        }
        return ResponseEntity
                .ok()
                .eTag(String.valueOf(offerRepository.all().hashCode()))
                .cacheControl(CacheControl.noCache())
                .body(offerRepository.all());
    }

    @GetMapping(value = "/offers", produces = "application/vnd.cards2+json")
    public ResponseEntity getOffersWithNames() {
        return ResponseEntity
                .ok()
                .eTag(String.valueOf(offerV2Repository.all().hashCode()))
                .cacheControl(CacheControl.noCache())
                .body(offerV2Repository.all());
    }

    @GetMapping("/offers/{id}")
    public Long getOffer(@PathVariable("id") int offerId) {
        return null;
    }

}

class Offer {

    private  int offerId;
    private  int fee;
    private  String name;


    Offer(int offerId, int fee) {
        this.offerId = offerId;
        this.fee = fee;
    }

    public int getOfferId() {
        return offerId;
    }

    public void setOfferId(int offerId) {
        this.offerId = offerId;
    }

    public int getFee() {
        return fee;
    }
}

class OfferV2 {

    private  int offerId;
    private  int fee;
    private  String name;


    OfferV2(int offerId, int fee, String name) {
        this.offerId = offerId;
        this.fee = fee;
        this.name = name;
    }

    public int getOfferId() {
        return offerId;
    }

    public void setOfferId(int offerId) {
        this.offerId = offerId;
    }

    public int getFee() {
        return fee;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}


@RestController
class CardApplicationController {

    private final Map<Integer, Application> apps = new ConcurrentHashMap<>();


    @PostMapping("/applications")
    public ResponseEntity applyForCard(@RequestBody ApplyForCard cardApplicationCommand) {
        int id = new Random().nextInt();
        apps.put(id, new Application(
                cardApplicationCommand.getPesel(),
                cardApplicationCommand.getOfferId()));
        return ResponseEntity
                .created(ServletUriComponentsBuilder
                        .fromCurrentRequest()
                        .replacePath("/applications/{id}")
                        .buildAndExpand(id)
                        .toUri())
                .build();
    }

    @GetMapping("/applications/{id}")
    public ResponseEntity applicationStatus(@PathVariable Integer id) {
        if(!apps.containsKey(id)) {
            return ResponseEntity.notFound().build();
        }

        return ResponseEntity.ok(new ApplicationWithTransitions(apps.get(id)));
    }

    @PutMapping("/applications/{id}")
    public ResponseEntity uploadScan(@RequestBody UploadIdScan uploadIdScanCommand) {
        if(!apps.containsKey(uploadIdScanCommand.getApplicationId())) {
            return ResponseEntity.notFound().build();
        }

        Application app = apps.get(uploadIdScanCommand.getApplicationId());
        app.setScanId(uploadIdScanCommand.getContent());
        return ResponseEntity.ok(app);
    }

}

class ApplyForCard {

    private String pesel;
    private int offerId;

    ApplyForCard() {

    }

    ApplyForCard(String pesel, int offerId) {
        this.pesel = pesel;
        this.offerId = offerId;
    }

    public String getPesel() {
        return pesel;
    }

    public void setPesel(String pesel) {
        this.pesel = pesel;
    }

    public int getOfferId() {
        return offerId;
    }
}

class UploadIdScan {

    private String content;
    private int applicationId;

    UploadIdScan() {

    }

    UploadIdScan(String content, int applicationId) {

        this.content = content;
        this.applicationId = applicationId;
    }


    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public int getApplicationId() {
        return applicationId;
    }

    public void setApplicationId(int applicationId) {
        this.applicationId = applicationId;
    }
}


class ApplicationWithTransitions extends ResourceSupport {

    private Application application;

    ApplicationWithTransitions(Application application) {
        this.application = application;
        if(application.getScanId().isEmpty()) {
            add(linkTo(
                    methodOn(CardApplicationController.class)
                    .uploadScan(
                            new UploadIdScan( "anyScan", 1)))
                    .withRel("Upload scan"));
        }
    }

    public Application getApplication() {
        return application;
    }
}


class Application {

    private String pesel;
    private int offerId;
    private String scanId;

    Application(String pesel, int offerId, String scanId) {
        this.pesel = pesel;
        this.offerId = offerId;
        this.scanId = scanId;
    }

    Application(String pesel, int offerId) {
        this.pesel = pesel;
        this.offerId = offerId;
        this.scanId = "";
    }

    public String getPesel() {
        return pesel;
    }

    public void setPesel(String pesel) {
        this.pesel = pesel;
    }

    public int getOfferId() {
        return offerId;
    }

    public String getScanId() {
        return scanId;
    }

    public void setScanId(String scanId) {
        this.scanId = scanId;
    }
}



