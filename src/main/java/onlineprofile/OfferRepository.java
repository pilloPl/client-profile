package onlineprofile;

import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Repository
public class OfferRepository {

    private final Map<Integer, Offer> offers = new ConcurrentHashMap<>();

    public void put(Integer id, Offer offer) {
        offers.put(id, offer);
    }

    public Offer get(Integer id) {
        return offers.get(id);
    }

    public boolean noOffers() {
        return offers.isEmpty();
    }

    public Collection<Offer> all() {
        return offers.values();
    }
}
