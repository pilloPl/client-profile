package onlineprofile;

import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Repository
public class OfferRepositoryV2 {

    private final Map<Integer, OfferV2> offers = new ConcurrentHashMap<>();

    public void put(Integer id, OfferV2 offer) {
        offers.put(id, offer);
    }

    public OfferV2 get(Integer id) {
        return offers.get(id);
    }

    public boolean noOffers() {
        return offers.isEmpty();
    }

    public Collection<OfferV2> all() {
        return offers.values();
    }

}
